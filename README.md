# Lino block chain tracker
The current Lino blockchain tracker (https://tracker.lino.network) only displays very limited information, but a more comprehensive blockchain tracker with more information displayed can be developed. Lino aims to create a decentralized autonomous content economy by leveraging the blockchain technology. In this economy, content value can be recognized efficiently, and all contributors can be incentivized in a more direct and effective manner that helps promote long-term economic growth for individual creators and for content creation generally. 
The main idea of the project is not just to create a regular blockchain tracker, but to create a user account with the possibility of tracking parameters such as the number of subscribers, donations and rewards. 

# Implementation
At this stage, the project allows you to track the number of transactions in blocks, the history of the user's balance, his rewards, information about the account and validators. You can get all the parameters of the blockchain. There is also the possibility of tracking the number of subscribers (this parameter in most cases is null, therefore it is currently disabled). Project was implemented as a web-site. 

# Technology stack
- Ubuntu 16.04LTS
- Golang 1.10
- Library lino-go
- HTML, CSS, JS
- Bootstrap 4

# Source code
My source code is in lino-go/example/web.go and lino-go/examle/html/*.* (all files)

# How to run demo: 
- docker build .
- docker run {$image name}  
We need get docker ID to get container localhost IP
- sudo docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' {$image name}  

And in the browser: 
- {IP address}:8080